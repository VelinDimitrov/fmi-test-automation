package systemtest.helper;

public class WebConfig {

	private WebConfig() {
		// cannot be instantiated
	}
	
	static {	
		DEFAULT_USER = "";
		SECOND_USER = " ";
		DEFAULT_PASSWORD = "";
		BASE_URL = "https://www.ebay.com/";
	}
	
	public static final String DEFAULT_USER;
	public static final String SECOND_USER;
	public static final String DEFAULT_PASSWORD;
	public static final String BASE_URL;
}
