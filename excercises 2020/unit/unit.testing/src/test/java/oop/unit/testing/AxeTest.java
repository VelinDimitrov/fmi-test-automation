package oop.unit.testing;

import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.junit.Test;

import impl.Axe;
import impl.Dummy;

public class AxeTest {

	@Test
	public void testAxeLosesDurabilityAfterAtack() {
		Axe axe = new Axe(10, 10);
		assertThat(axe.getDurabilityPoints(), Matchers.equalTo(10));
		Dummy target = new Dummy(10, 10);
		axe.attack(target);
		assertThat(axe.getDurabilityPoints(), Matchers.equalTo(9));
	}
	
	@Test
	public void testAttackWithBrokenWeapon() {
		Axe axe = new Axe(10, 0);
		Dummy target = new Dummy(10, 10);
		try {
		axe.attack(target);
		}catch(IllegalStateException ise){
			assertThat(ise.getMessage(),Matchers.comparesEqualTo("Axe is broken."));
		}	
	}
	
}
