package oop.unit.testing;

import static org.junit.Assert.assertThat;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import impl.Axe;
import impl.Hero;
import interfaces.Target;
import interfaces.Weapon;

public class HeroTest {

	public static final int ATTACK = 10;
	public static final int DURABILITY = 10;
	public static final int HEALTH = 10;
	public static final int XP = 10;
	private static final String HERO_NAME = "s0g00d";
	private static final int TARGET_XP = 5;
	private static final int NO_XP = 0;
	
	private Weapon weapon;
	private Target target;
	
	@Before
	public void executeBeforeEachTest() {
		this.weapon = Mockito.mock(Axe.class);
		this.target = Mockito.mock(Target.class);
		Mockito.when(this.target.giveExperience()).thenReturn(TARGET_XP);
	}
	
	@Test
	public void testHeroAtackGainsXPIfTargetIsDead() {
		Mockito.when(this.target.isDead()).thenReturn(true);
		Hero hero = new Hero(HERO_NAME, this.weapon);
		assertThat(hero.getExperience(), Matchers.equalTo(NO_XP));
		hero.attack(target);
		assertThat(hero.getExperience(), Matchers.equalTo(TARGET_XP));
		hero.attack(target);
		assertThat(hero.getExperience(), Matchers.equalTo(TARGET_XP*2));
	}
	
	@Test
	public void testHeroCannotGainXPIfTargetIsNotDead() {
		Mockito.when(this.target.isDead()).thenReturn(false);
		Hero hero = new Hero(HERO_NAME, this.weapon);
		assertThat(hero.getExperience(), Matchers.equalTo(NO_XP));
		hero.attack(target);
		assertThat(hero.getExperience(), Matchers.equalTo(NO_XP));
	}
}
