package oop.unit.testing;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import impl.Axe;
import impl.Dummy;

public class TargetTest {
	
	@Test
	public void testIsTargetDead() {
		Axe axe = new Axe(5, 5);
		Dummy target = new Dummy(10, 10);
		assertFalse(target.isDead());
		axe.attack(target);
		assertFalse(target.isDead());
		axe.attack(target);
		assertTrue(target.isDead());
	}
}
