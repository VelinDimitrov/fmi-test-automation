package sabev.test.test.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.NeedsLocalLogs;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FormsPage extends HomePage<FormsPage>{
	private WebDriver driver;
	@FindBy(xpath = "//input[@id='firstName']")
	private WebElement firstNameInputText;

	@FindBy(id = "lastName")
	private WebElement lastNameInputText;
	
	@FindBy(css = "label[for='gender-radio-2']")
	private WebElement secondGenderRadioButton;
	
	@FindBy(css="input[placeholder='name@example.com']")
	private WebElement emailInput;
	
	@FindBy(id="userNumber")
	private WebElement phoneInput;
	
	@FindBy(css="textarea[placeholder='Current Address']")
	private WebElement addressInput;
	
	//@FindBy(xpath="//input[starts-with(@id,'hobbies-checkbox')]/../label")
	@FindBy(xpath="//input[@id='hobbies-checkbox-1' or @id='hobbies-checkbox-2' ]/../label")
	private List<WebElement> hobbies;
	
	@FindBy(xpath="//button[@id='submit' and @type='submit']")
	private WebElement submitBt;
	
	@FindBy(id="subjectsInput")
	private WebElement subjectsInput;
	
	@FindBy(xpath="//input[@id='react-select-3-input']")
	private WebElement stateSelect;
	
	@FindBy(id="react-select-4-input")
	private WebElement citySelect;

	public FormsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	

	
	public AutomatedFormModalPage fillForm(String email, String phone, String address, String subject, String state, String city) throws InterruptedException {
		clickFemaleGenderRadioButton();
		Thread.sleep(500);
		emailInput.sendKeys(email);
		Thread.sleep(500);
		phoneInput.sendKeys(phone);
		Thread.sleep(500);
		addressInput.click();
		Thread.sleep(500);
		addressInput.sendKeys(address);
		Thread.sleep(500);
		selectAllHobbies();
		
		subjectsInput.sendKeys(subject);
		Thread.sleep(500);
		subjectsInput.sendKeys(Keys.ENTER);	
		Thread.sleep(500);		
		
		stateSelect.sendKeys(state);
		Thread.sleep(500);
		stateSelect.sendKeys(Keys.ENTER);
		Thread.sleep(500);
		
		citySelect.sendKeys(city);
		Thread.sleep(500);
		citySelect.sendKeys(Keys.ENTER);
		
		
		Thread.sleep(500);
		return submitForm();
		
	}

	public void fillFirstName(String firstName) throws InterruptedException {
		this.firstNameInputText.clear();
		Thread.sleep(500);
		this.firstNameInputText.sendKeys(firstName);
		Thread.sleep(500);
	}

	public void fillLastName(String lastName) throws InterruptedException {
		this.lastNameInputText.clear();
		Thread.sleep(500);
		this.lastNameInputText.sendKeys(lastName);
		Thread.sleep(500);
	}
	
	public String getFirstNameInputValue() {
		return this.firstNameInputText.getAttribute("value");
	}
	
	public void clickFemaleGenderRadioButton() throws InterruptedException {
		this.secondGenderRadioButton.click();
		Thread.sleep(500);
	}
	
	public void selectAllHobbies() throws InterruptedException {
		for (WebElement webElement : hobbies) {
			webElement.click();
			Thread.sleep(500);
		}
	}
	public AutomatedFormModalPage submitForm() {
		submitBt.submit();
		return new AutomatedFormModalPage(driver);
	}
	

	
}
