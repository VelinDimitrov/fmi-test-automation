package sabev.test.test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public abstract class BasePage<T> {
	private WebDriver webDriver;
	public static final String BASE_URL = "https://demoqa.com/";
	
	public BasePage(WebDriver driver) {
		webDriver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public T openHomePage() {
		webDriver.navigate().to(BASE_URL);
		return (T) new HomePage(webDriver);
	}

}
