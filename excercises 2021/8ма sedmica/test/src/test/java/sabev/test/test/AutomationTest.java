package sabev.test.test;

import static java.lang.Thread.sleep;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.seljup.SeleniumExtension;
import sabev.test.test.pages.AutomatedFormModalPage;
import sabev.test.test.pages.FormsPage;
import sabev.test.test.pages.HomePage;

@ExtendWith(SeleniumExtension.class)
public class AutomationTest {
	


	@Test
	public void automateForm(ChromeDriver driver) throws InterruptedException {
		FormsPage automatePracticeFormPage =  new HomePage(driver).navigateToFormsPage();
				
		automatePracticeFormPage.fillFirstName("Tosho");		
		automatePracticeFormPage.fillLastName("Toshev");

		assertEquals("Student Registration Form",driver.findElementByXPath("//h5[contains(text(),'Student Registration')]").getText());

		assertEquals("Tosho", automatePracticeFormPage.getFirstNameInputValue());

		AutomatedFormModalPage automatedFormModalPage
		 = automatePracticeFormPage.fillForm("gsabev@email.com", "0123456789", "plovdiv", "Eng", "N", "G");

		
		assertThat(automatedFormModalPage.getHeadingText()).isEqualTo("Thanks for submitting the form");
		assertThat(automatedFormModalPage.getMobileText()).isEqualTo("0123456789");
		assertThat(automatedFormModalPage.getGenderText()).isEqualTo("Female");
		sleep(1000);
	}
	
}
