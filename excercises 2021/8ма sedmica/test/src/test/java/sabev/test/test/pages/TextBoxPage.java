package sabev.test.test.pages;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TextBoxPage extends ElementsPage{
	
	private WebDriver driver;
	
	@FindBy(id="userName")
	private WebElement userName;
	
	@FindBy(id="userEmail")
	private WebElement userEmail;
	
	
	@FindBy(id="currentAddress")
	private WebElement currentAddress;
	
	@FindBy(id="permanentAddress")
	private WebElement permanentAddress;
	
	@FindBy(id="submit")
	private WebElement submit;
	
	
		
	public TextBoxPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	
	public TextBoxPage addUserName(String username) {
		userName.sendKeys(username);
		return this;
	}
	
	public TextBoxPage addUserEmail(String useremail) {
		userEmail.sendKeys(useremail);
		return this;
	}

	public TextBoxPage addCurrentAddress(String currentaddress) {
		currentAddress.sendKeys(currentaddress);
		return this;
	}

	public TextBoxPage addPermanentAddress(String permanentaddress) {
		permanentAddress.sendKeys(permanentaddress);
		return this;
	}

	public TextBoxPage clickOnSubmit() {
		submit.click();
		return this;
	}
	
	public String getSubmitedName() {
		
		try {
			return driver.findElement(By.id("name")).getText();
		} catch (Exception e) {
			return "NameIsEmpty";
		}
	}

	public TextBoxPage isOnTextBoxPage() {
		try {
			driver.findElement(By.xpath("//div[contains(text(),'Text Box')]"));
		} catch (Exception e) {
			fail("Not on page");
		}
		return this;
	}
	
	public String submitForm(String name, String email, String currentaddress, String permanentaddress) {
			addUserEmail(email);
			addCurrentAddress(currentaddress);
			addUserName(name);
			addPermanentAddress(permanentaddress);
			clickOnSubmit();
			return getSubmitedName();
			
	}
}
