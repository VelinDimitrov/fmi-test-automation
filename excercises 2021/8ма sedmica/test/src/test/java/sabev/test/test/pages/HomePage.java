package sabev.test.test.pages;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage <T> extends BasePage<HomePage>{
	private WebDriver driver;
	
	public HomePage(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	public ElementsPage navigateToElementPage() {
		driver.navigate().to(BASE_URL + "elements");
		return  new ElementsPage(driver);
	}
	
	public AlertsWindowsPage navigateToAlertsWindowsPage() {
		driver.navigate().to(BASE_URL + "alertsWindows");
		return new AlertsWindowsPage(driver);
	}
	
	public FormsPage navigateToFormsPage() {
		driver.navigate().to(BASE_URL + "automation-practice-form");
		return new FormsPage(driver);
	}
	
	public T isPageExpanded() {
		try {
			driver.findElement(By.xpath("//div[@class='element-list collapse show']"));
		} catch (Exception e) {
			fail("Not expanded");
		}
		
		return (T) this;
	}
	
}
