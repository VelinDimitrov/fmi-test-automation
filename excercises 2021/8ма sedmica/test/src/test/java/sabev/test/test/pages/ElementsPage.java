package sabev.test.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class ElementsPage extends HomePage<ElementsPage>{
	private WebDriver driver;
	
	public ElementsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	public TextBoxPage navaigateToTextBox() {
		driver.navigate().to(BASE_URL+"text-box");
		return new TextBoxPage(driver);
	}
	

	
}
