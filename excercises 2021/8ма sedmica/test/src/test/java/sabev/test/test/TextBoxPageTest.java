package sabev.test.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.seljup.SeleniumExtension;
import sabev.test.test.pages.HomePage;

@ExtendWith(SeleniumExtension.class)
public class TextBoxPageTest {

	@Test
	public void testSubmitingForm(ChromeDriver driver) throws InterruptedException {
		String name = "gsabev";
		assertThat(
				new HomePage(driver)
				.navigateToElementPage()
				.navaigateToTextBox()
				.isOnTextBoxPage()
				.submitForm(name, "email.gsabev@email.com", "plovdiv", "plovdiv"))
		.isEqualTo("Name:"+name);
	}
	
	@Test
	public void testSubmitingFormWhitoutName(ChromeDriver driver) throws InterruptedException {
		assertThat(
				new HomePage(driver)
				.navigateToElementPage()
				.navaigateToTextBox()
				.isOnTextBoxPage()
				.submitForm("", "email.gsabev@email.com", "plovdiv", "plovdiv"))
		.isEqualTo("NameIsEmpty");
	}

}
