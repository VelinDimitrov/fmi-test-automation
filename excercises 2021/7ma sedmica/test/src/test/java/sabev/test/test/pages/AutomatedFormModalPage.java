package sabev.test.test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AutomatedFormModalPage {
	
	@FindBy(id="example-modal-sizes-title-lg")
	private WebElement heading;
	
	@FindBy(xpath="//div[@class='modal-body']//table/tbody//td[contains(text(),'Mobile')]/following-sibling::td")
	private WebElement mobileTableRowValue;
	
	@FindBy(xpath="//div[@class='modal-body']//table/tbody//td[contains(text(),'Gender')]/following-sibling::td")
	private WebElement genderTableRowValue;
	
	public AutomatedFormModalPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	private String getFieldText(WebElement element) {
		return element.getText();
	}
	
	public String getHeadingText() {
		return getFieldText(heading);
	}
	
	public String getMobileText() {
		return getFieldText(mobileTableRowValue);
	}
	
	public String getGenderText() {
		return getFieldText(genderTableRowValue);
	}
}
