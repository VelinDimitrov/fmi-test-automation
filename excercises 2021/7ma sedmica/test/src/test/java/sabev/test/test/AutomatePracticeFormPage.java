package sabev.test.test;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AutomatePracticeFormPage {
	
	@FindBy(xpath = "//input[@id='firstName']")
	private WebElement firstNameInputText;

	@FindBy(id = "lastName")
	private WebElement lastNameInputText;
	
	@FindBy(css = "label[for='gender-radio-2']")
	private WebElement secondGenderRadioButton;
	
	@FindBy(css="input[placeholder='name@example.com']")
	private WebElement emailInput;
	
	@FindBy(id="userNumber")
	private WebElement phoneInput;
	
	@FindBy(css="textarea[placeholder='Current Address']")
	private WebElement addressInput;
	
	//@FindBy(xpath="//input[starts-with(@id,'hobbies-checkbox')]/../label")
	@FindBy(xpath="//input[@id='hobbies-checkbox-1' or @id='hobbies-checkbox-2' ]/../label")
	private List<WebElement> hobbies;
	
	@FindBy(xpath="//button[@id='submit' and @type='submit']")
	private WebElement submitBt;
	
	@FindBy(id="subjectsInput")
	private WebElement subjectsInput;
	
	@FindBy(xpath="//input[@id='react-select-3-input']")
	private WebElement stateSelect;
	
	@FindBy(id="react-select-4-input")
	private WebElement citySelect;

	public AutomatePracticeFormPage(WebDriver driver) {
		System.out.println();
		PageFactory.initElements(driver, this);
		System.out.println();
	}
	
	public void fillForm() throws InterruptedException {
		clickFemaleGenderRadioButton();
		Thread.sleep(500);
		emailInput.sendKeys("blabla@abv.bg");
		Thread.sleep(500);
		phoneInput.sendKeys("0123456789");
		Thread.sleep(500);
		addressInput.click();
		Thread.sleep(500);
		addressInput.sendKeys("Dummy text");
		Thread.sleep(500);
		selectAllHobbies();
		
		subjectsInput.sendKeys("Eng");
		Thread.sleep(500);
		subjectsInput.sendKeys(Keys.ENTER);	
		Thread.sleep(500);		
		
		stateSelect.sendKeys("N");
		Thread.sleep(500);
		stateSelect.sendKeys(Keys.ENTER);
		Thread.sleep(500);
		
		citySelect.sendKeys("G");
		Thread.sleep(500);
		citySelect.sendKeys(Keys.ENTER);
		
		
		Thread.sleep(500);
		submitBt.submit();
	}

	public void fillFirstName(String firstName) throws InterruptedException {
		this.firstNameInputText.clear();
		Thread.sleep(500);
		this.firstNameInputText.sendKeys(firstName);
		Thread.sleep(500);
	}

	public void fillLastName(String lastName) throws InterruptedException {
		this.lastNameInputText.clear();
		Thread.sleep(500);
		this.lastNameInputText.sendKeys(lastName);
		Thread.sleep(500);
	}
	
	public String getFirstNameInputValue() {
		return this.firstNameInputText.getAttribute("value");
	}
	
	public void clickFemaleGenderRadioButton() throws InterruptedException {
		this.secondGenderRadioButton.click();
		Thread.sleep(500);
	}
	
	public void selectAllHobbies() throws InterruptedException {
		for (WebElement webElement : hobbies) {
			webElement.click();
			Thread.sleep(500);
		}
	}
}
