package sabev.test.test;

import static java.lang.Thread.sleep;
import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.seljup.SeleniumExtension;
import sabev.test.test.pages.AutomatedFormModalPage;

@ExtendWith(SeleniumExtension.class)
public class AutomationTest {
	
	private AutomatePracticeFormPage automatePracticeFormPage;
	private AutomatedFormModalPage automatedFormModalPage;
	private FirefoxDriver driver;
	
	@BeforeEach
	public void setUp(FirefoxDriver driver) {
		this.driver=driver;
		this.driver.manage().window().maximize();
		this.driver.navigate().to("https://demoqa.com/automation-practice-form");
		automatePracticeFormPage = new AutomatePracticeFormPage(driver);
		automatedFormModalPage = new AutomatedFormModalPage(driver);
	}

	@Test
	public void automateForm() throws InterruptedException {
		automatePracticeFormPage.fillFirstName("Gosho");
		
		automatePracticeFormPage.fillFirstName("Tosho");
		
		automatePracticeFormPage.fillLastName("Toshev");

		assertEquals("Student Registration Form",driver.findElementByXPath("//h5[contains(text(),'Student Registration')]").getText());

		assertEquals("Tosho", automatePracticeFormPage.getFirstNameInputValue());

		automatePracticeFormPage.fillForm();

		assertEquals("Thanks for submitting the form",
				automatedFormModalPage.getHeadingText());
		
		assertEquals("0123456789", automatedFormModalPage.getMobileText());
		assertEquals("Female", automatedFormModalPage.getGenderText());
		sleep(1000);
	}
	
	@Test
	public void automateFormtest2() throws InterruptedException {

		automatePracticeFormPage.fillFirstName("Gosho");
		
		automatePracticeFormPage.fillFirstName("Tosho");
		
		automatePracticeFormPage.fillLastName("Toshev");

		assertEquals("Student Registration Form",driver.findElementByXPath("//h5[contains(text(),'Student Registration')]").getText());

		assertEquals("Tosho", automatePracticeFormPage.getFirstNameInputValue());

		automatePracticeFormPage.fillForm();

		assertEquals("Thanks for submitting the form",
				automatedFormModalPage.getHeadingText());
		
		assertEquals("0123456789", automatedFormModalPage.getMobileText());
		assertEquals("Female", automatedFormModalPage.getGenderText());
		sleep(1000);

	}

}
