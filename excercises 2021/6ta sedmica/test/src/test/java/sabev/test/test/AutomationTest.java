package sabev.test.test;

import static java.lang.Thread.sleep;
import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.seljup.SeleniumExtension;

@ExtendWith(SeleniumExtension.class)
public class AutomationTest {

	@Test
	public void testLocalFirefox(FirefoxDriver driver) throws InterruptedException {
		// driver.get("https://demoqa.com/automation-practice-form");
		driver.manage().window().maximize();
		// driver.navigate().to("https://www.google.com/");
		sleep(2000);
		driver.navigate().to("https://demoqa.com/automation-practice-form");
//		sleep(2000);
//		driver.navigate().back();
//		sleep(2000);
//		driver.navigate().forward();
//		sleep(2000);
//		driver.navigate().refresh();
//		sleep(2000);
//		driver.findElement(By.id("firstName")).sendKeys("Tosho");
//		sleep(1000);
//		driver.findElement(By.id("lastName")).sendKeys("Toshev");
		sleep(2000);
		assertEquals("Student Registration Form",driver.findElementByXPath("//h5[contains(text(),'Student Registration')]").getText());
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Tosho");
		sleep(1000);
		driver.findElementByXPath("//input[@id='lastName']").sendKeys("Toshev");
		sleep(1000);
		assertEquals("Tosho", driver.findElement(By.id("firstName")).getAttribute("value"));
		assertNotEquals("Toshofdsfds", driver.findElement(By.id("firstName")).getAttribute("value"));
		driver.findElement(By.cssSelector("input[placeholder='name@example.com']")).sendKeys("blabla@abv.bg");
		sleep(1000);
		driver.findElement(By.className("custom-control-input"));
		sleep(1000);

		driver.findElementById("userNumber").sendKeys("0123456789");
		sleep(1000);

		driver.findElement(By.cssSelector("label[for='gender-radio-2']")).click();
		sleep(1000);
		driver.findElement(By.cssSelector("label[for='hobbies-checkbox-3']")).click();
		sleep(1000);
		selectSubject(driver, "Eng");
		selectSubject(driver, "Comp");
		
		driver.findElement(By.cssSelector("textarea[placeholder='Current Address']")).click();
		sleep(1000);
	//	driver.findElement(By.cssSelector("textarea[placeholder='Current Address']")).sendKeys("Dummy text");
		driver.findElementByXPath("//textarea[starts-with(@id,'currentAdd')]").sendKeys("Dummy Text");
		sleep(1000);

	//	driver.findElement(By.cssSelector("button[id='submit']")).click();
		driver.findElementByXPath("//button[@id='submit' and @type='submit']").click();
		sleep(1000);

		assertEquals("Thanks for submitting the form",
				driver.findElementById("example-modal-sizes-title-lg").getText());
		sleep(1000);

		sleep(2000);
	}

	private void selectSubject(FirefoxDriver driver, String subject) throws InterruptedException {
		driver.findElement(By.id("subjectsInput")).sendKeys(subject);
		sleep(1000);
		driver.findElement(By.id("subjectsInput")).sendKeys(Keys.ENTER);
		sleep(1000);
	}
//	//tagname[@attribute='value']/../
	// /html/body/div[2]/div[1]/div/h4[1]/b/html[1]/body[1]/div[2]/div[1]/div[1]/h4[1]/b[1] 
}
